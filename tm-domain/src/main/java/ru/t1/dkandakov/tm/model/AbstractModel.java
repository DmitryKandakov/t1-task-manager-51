package ru.t1.dkandakov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractModel implements Serializable {

    @Id
    @Getter
    @Setter
    @NotNull
    protected String id = UUID.randomUUID().toString();

}