package ru.t1.dkandakov.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.ProjectDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListResponse extends AbstractProjectResponse {

    @Nullable
    private List<ProjectDTO> projects;

    public ProjectListResponse(@Nullable List<ProjectDTO> projects) {
        this.projects = projects;
    }

}
