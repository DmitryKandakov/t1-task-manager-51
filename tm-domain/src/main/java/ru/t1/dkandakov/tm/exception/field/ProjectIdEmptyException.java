package ru.t1.dkandakov.tm.exception.field;

public final class ProjectIdEmptyException extends AbstractFildException {

    public ProjectIdEmptyException() {
        super("Error! Project Id is empty...");
    }

}
