package ru.t1.dkandakov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

public interface IUserOwnedServiceDTO<M extends AbstractUserOwnedModelDTO> extends IServiceDTO<M> {

    @Nullable
    M add(
            @Nullable String userId,
            @Nullable M model
    );

    @NotNull
    List<M> findAll(@Nullable String userId);

    @Nullable
    M findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    M findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    M removeOne(
            @Nullable String userId,
            @Nullable M model
    );

    @Nullable
    M removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @Nullable
    M removeOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    );

    void removeAll(@Nullable String userId);

    long getSize(@Nullable String userId);

    boolean existsById(
            @Nullable String userId,
            @Nullable String id
    );

}
