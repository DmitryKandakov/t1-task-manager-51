package ru.t1.dkandakov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.api.repository.model.IUserOwnerRepository;
import ru.t1.dkandakov.tm.model.AbstractUserOwnedModel;
import ru.t1.dkandakov.tm.model.User;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnerRepository<M> {

    protected AbstractUserOwnerRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final M model) {
        model.setUser(entityManager.find(User.class, userId));
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        model.setUser(entityManager.find(User.class, userId));
        entityManager.merge(model);
    }

}
