package ru.t1.dkandakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkandakov.tm.api.service.IDatabaseProperty;
import ru.t1.dkandakov.tm.api.service.IPropertyService;
import ru.t1.dkandakov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class PropertyServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IDatabaseProperty databaseProperty = new PropertyService();

    @Test
    public void testPropertyGetServerHost() {
        Assert.assertNotNull(propertyService.getServerHost());
    }

    @Test
    public void testPropertyGetServerPort() {
        Assert.assertNotNull(propertyService.getServerPort());
    }

    @Test
    public void testPropertyGetSessionKey() {
        Assert.assertNotNull(propertyService.getSessionKey());
    }

    @Test
    public void testPropertyGetSessionTimeout() {
        Assert.assertNotNull(propertyService.getSessionTimeout());
    }

    @Test
    public void testPropertyGetDatabaseLogin() {
        Assert.assertNotNull(databaseProperty.getDatabaseLogin());
    }

    @Test
    public void testPropertyGetDatabasePassword() {
        Assert.assertNotNull(databaseProperty.getDatabasePassword());
    }

    @Test
    public void testPropertyGetDatabaseUrl() {
        Assert.assertNotNull(databaseProperty.getDatabaseUrl());
    }

    @Test
    public void testPropertyGetDatabaseDriver() {
        Assert.assertNotNull(databaseProperty.getDatabaseDriver());
    }

    @Test
    public void testPropertyGetDatabaseDialect() {
        Assert.assertNotNull(databaseProperty.getDatabaseDialect());
    }

    @Test
    public void testPropertyGetDatabaseHBM2DDLAuto() {
        Assert.assertNotNull(databaseProperty.getDatabaseHBM2DDLAuto());
    }

    @Test
    public void testPropertyGetDatabaseShowSQL() {
        Assert.assertNotNull(databaseProperty.getDatabaseShowSQL());
    }

}
