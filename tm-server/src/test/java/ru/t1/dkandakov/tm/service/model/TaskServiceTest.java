package ru.t1.dkandakov.tm.service.model;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.dkandakov.tm.api.service.IConnectionService;
import ru.t1.dkandakov.tm.api.service.IPropertyService;
import ru.t1.dkandakov.tm.api.service.model.IProjectService;
import ru.t1.dkandakov.tm.api.service.model.ITaskService;
import ru.t1.dkandakov.tm.api.service.model.IUserService;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.enumerated.TaskSort;
import ru.t1.dkandakov.tm.exception.entity.EntityNotFoundException;
import ru.t1.dkandakov.tm.exception.entity.StatusEmptyException;
import ru.t1.dkandakov.tm.exception.entity.ValueIsNullException;
import ru.t1.dkandakov.tm.exception.field.*;
import ru.t1.dkandakov.tm.marker.UnitCategory;
import ru.t1.dkandakov.tm.migration.AbstractSchemeTest;
import ru.t1.dkandakov.tm.model.Task;
import ru.t1.dkandakov.tm.service.ConnectionService;
import ru.t1.dkandakov.tm.service.PropertyService;

import java.util.List;

import static ru.t1.dkandakov.tm.constant.model.ProjectTestData.USER1_PROJECT1;
import static ru.t1.dkandakov.tm.constant.model.TaskTestData.*;
import static ru.t1.dkandakov.tm.constant.model.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest extends AbstractSchemeTest {

    @Getter
    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private static IProjectService projectService;

    @NotNull
    private static ITaskService taskService;

    @NotNull
    private static IUserService userService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void initConnectionService() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog.xml");
        liquibase.dropAll();
        liquibase.update("sheme");
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        projectService = new ProjectService(connectionService);
        taskService = new TaskService(connectionService, projectService);
        userService = new UserService(connectionService, propertyService);
    }

    @Before
    public void clear() {
        userService.removeAll();
        userService.add(ADMIN);
        userService.add(USER1);
        userService.add(USER2);
        projectService.add(USER1_PROJECT1);
    }

    @Test
    public void testTaskAdd() {
        Assert.assertNotNull(taskService.add(USER1_TASK1));
        Assert.assertEquals(USER1_TASK1.getId(), taskService.findAll().get(0).getId());

        thrown.expect(EntityNotFoundException.class);
        taskService.add(NULL_TASK);
    }

    @Test
    public void testTaskAddByUserId() {
        Assert.assertNotNull(taskService.add(USER1.getId(), USER1_TASK1));
        Assert.assertEquals(USER1_TASK1.getId(), taskService.findAll().get(0).getId());

        thrown.expect(UserIdEmptyException.class);
        taskService.add(NULL_USER_ID, USER1_TASK1);
        thrown.expect(EntityNotFoundException.class);
        taskService.add(USER1.getId(), NULL_TASK);
    }

    @Test
    public void testTaskAddModels() {
        Assert.assertNotNull(taskService.add(ADMIN_TASK_LIST));
        Assert.assertEquals(ADMIN_TASK_LIST.get(0).getId(), taskService.findAll().get(0).getId());
        Assert.assertEquals(ADMIN_TASK_LIST.get(1).getId(), taskService.findAll().get(1).getId());

        thrown.expect(ValueIsNullException.class);
        taskService.add(NULL_TASK_LIST);
    }

    @Test
    public void testTaskSet() {
        Assert.assertNotNull(taskService.set(ADMIN_TASK_LIST));
        Assert.assertEquals(ADMIN_TASK_LIST.get(0).getId(), taskService.findAll().get(0).getId());
        Assert.assertEquals(ADMIN_TASK_LIST.get(1).getId(), taskService.findAll().get(1).getId());
        Assert.assertNotNull(taskService.set(USER2_TASK_LIST));
        Assert.assertEquals(USER2_TASK_LIST.get(0).getId(), taskService.findAll().get(0).getId());

        thrown.expect(ValueIsNullException.class);
        taskService.add(NULL_TASK_LIST);
    }

    @Test
    public void testTaskFindAll() {
        taskService.add(USER1_TASK_LIST);
        @NotNull final List<Task> taskList = taskService.findAll();
        Assert.assertEquals(USER1_TASK_LIST.get(0).getId(), taskList.get(0).getId());
        Assert.assertEquals(USER1_TASK_LIST.get(1).getId(), taskList.get(1).getId());
        Assert.assertEquals(USER1_TASK_LIST.get(2).getId(), taskList.get(2).getId());
    }

    @Test
    public void testTaskFindAllByUserId() {
        taskService.add(USER1_TASK_LIST);
        taskService.add(ADMIN_TASK_LIST);
        @NotNull final List<Task> taskListUser1 = taskService.findAll(USER1.getId());
        Assert.assertEquals(USER1_TASK_LIST.get(0).getId(), taskListUser1.get(0).getId());
        Assert.assertEquals(USER1_TASK_LIST.get(1).getId(), taskListUser1.get(1).getId());
        Assert.assertEquals(USER1_TASK_LIST.get(2).getId(), taskListUser1.get(2).getId());
        @NotNull final List<Task> taskListAdmin = taskService.findAll(ADMIN.getId());
        Assert.assertEquals(ADMIN_TASK_LIST.get(0).getId(), taskListAdmin.get(0).getId());
        Assert.assertEquals(ADMIN_TASK_LIST.get(1).getId(), taskListAdmin.get(1).getId());

        thrown.expect(UserIdEmptyException.class);
        taskService.findAll(NULL_USER_ID);
    }

    @Test
    public void testTaskFindAllSortByUserId() {
        taskService.removeAll();
        taskService.add(ADMIN_TASK_LIST.get(1));
        taskService.add(USER1_TASK_LIST.get(0));
        taskService.add(ADMIN_TASK_LIST.get(0));
        taskService.add(USER1_TASK_LIST.get(1));
        @NotNull final TaskSort sort = TaskSort.BY_NAME;
        @NotNull final List<Task> taskListUser1 = taskService.findAll(USER1.getId(), sort);
        Assert.assertEquals(USER1_TASK_LIST.get(0).getId(), taskListUser1.get(0).getId());
        Assert.assertEquals(USER1_TASK_LIST.get(1).getId(), taskListUser1.get(1).getId());
        @NotNull final List<Task> taskListAdmin = taskService.findAll(ADMIN.getId(), sort);
        Assert.assertEquals(ADMIN_TASK_LIST.get(0).getId(), taskListAdmin.get(0).getId());
        Assert.assertEquals(ADMIN_TASK_LIST.get(1).getId(), taskListAdmin.get(1).getId());

        thrown.expect(UserIdEmptyException.class);
        taskService.findAll(NULL_USER_ID, sort);
    }

    @Test
    public void testTaskFindOneById() {
        taskService.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK2.getId(), taskService.findOneById(USER1_TASK2.getId()).getId());
        Assert.assertNull(taskService.findOneById("test-id"));

        thrown.expect(IdEmptyException.class);
        taskService.findOneById(NULL_TASK_ID);
    }

    @Test
    public void testTaskFindOneByIdByUserId() {
        taskService.add(USER1_TASK_LIST);
        taskService.add(ADMIN_TASK_LIST);
        Assert.assertEquals(USER1_TASK2.getId(), taskService.findOneById(USER1.getId(), USER1_TASK2.getId()).getId());
        Assert.assertEquals(ADMIN_TASK2.getId(), taskService.findOneById(ADMIN.getId(), ADMIN_TASK2.getId()).getId());
        Assert.assertNull(taskService.findOneById(USER1.getId(), ADMIN_TASK2.getId()));
        Assert.assertNull(taskService.findOneById(USER1.getId(), "test-id"));

        thrown.expect(UserIdEmptyException.class);
        taskService.findOneById(NULL_USER_ID, ADMIN_TASK2.getId());
        thrown.expect(IdEmptyException.class);
        taskService.findOneById(NULL_TASK_ID);
    }

    @Test
    public void testTaskFindOneByIndex() {
        taskService.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK2.getId(), taskService.findOneByIndex(1).getId());

        thrown.expect(IndexIncorrectException.class);
        taskService.findOneByIndex(-1);
    }

    @Test
    public void testTaskFindOneByIndexByUserId() {
        taskService.add(USER1_TASK_LIST);
        taskService.add(ADMIN_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.get(1).getId(), taskService.findOneByIndex(USER1.getId(), 1).getId());
        Assert.assertEquals(ADMIN_TASK_LIST.get(0).getId(), taskService.findOneByIndex(ADMIN.getId(), 0).getId());

        thrown.expect(UserIdEmptyException.class);
        taskService.findOneByIndex(NULL_USER_ID, 0);
        thrown.expect(IndexIncorrectException.class);
        taskService.findOneByIndex(USER1.getId(), -1);
    }

    @Test
    public void testTaskRemoveOne() {
        taskService.add(USER1_TASK_LIST);
        Assert.assertNotNull(taskService.findOneById(USER1_TASK2.getId()));
        Assert.assertNotNull(taskService.removeOne(USER1_TASK2));
        Assert.assertNull(taskService.findOneById(USER1_TASK2.getId()));

        thrown.expect(EntityNotFoundException.class);
        taskService.removeOne(NULL_TASK);
    }

    @Test
    public void testTaskRemoveOneByUserId() {
        taskService.add(USER1_TASK_LIST);
        taskService.add(USER2_TASK_LIST);
        Assert.assertNotNull(taskService.findOneById(USER1_TASK2.getId()));
        taskService.removeOne(USER2.getId(), USER1_TASK2);
        Assert.assertNotNull(taskService.findOneById(USER1_TASK2.getId()));
        taskService.removeOne(USER1.getId(), USER1_TASK2);
        Assert.assertNull(taskService.findOneById(USER1_TASK2.getId()));

        thrown.expect(UserIdEmptyException.class);
        taskService.removeOne(NULL_USER_ID, USER1_TASK2);
        thrown.expect(EntityNotFoundException.class);
        taskService.removeOne(USER1.getId(), NULL_TASK);
    }

    @Test
    public void testTaskRemoveOneById() {
        taskService.add(USER1_TASK_LIST);
        Assert.assertNotNull(taskService.findOneById(USER1_TASK2.getId()));
        Assert.assertNotNull(taskService.removeOneById(USER1_TASK2.getId()));
        Assert.assertNull(taskService.findOneById(USER1_TASK2.getId()));

        thrown.expect(IdEmptyException.class);
        taskService.removeOneById(NULL_TASK_ID);
    }

    @Test
    public void testTaskRemoveOneByIdByUserId() {
        taskService.add(USER1_TASK_LIST);
        taskService.add(USER2_TASK_LIST);
        Assert.assertNotNull(taskService.findOneById(USER1_TASK2.getId()));
        Assert.assertNull(taskService.removeOneById(USER2.getId(), USER1_TASK2.getId()));
        Assert.assertNotNull(taskService.findOneById(USER1_TASK2.getId()));
        Assert.assertNotNull(taskService.removeOneById(USER1.getId(), USER1_TASK2.getId()));
        Assert.assertNull(taskService.findOneById(USER1_TASK2.getId()));

        thrown.expect(UserIdEmptyException.class);
        taskService.removeOneById(NULL_USER_ID, USER1_TASK2.getId());
        thrown.expect(IdEmptyException.class);
        taskService.removeOneById(USER2.getId(), NULL_TASK_ID);
    }

    @Test
    public void testTaskRemoveOneByIndex() {
        taskService.add(USER1_TASK1);
        Assert.assertNotNull(taskService.findOneById(USER1_TASK1.getId()));
        Assert.assertNotNull(taskService.removeOneByIndex(0));
        Assert.assertNull(taskService.findOneById(USER1_TASK1.getId()));

        thrown.expect(IndexIncorrectException.class);
        taskService.removeOneByIndex(-1);
    }

    @Test
    public void testTaskRemoveOneByIndexByUserId() {
        taskService.add(USER1_TASK1);
        taskService.add(USER2_TASK1);
        Assert.assertNotNull(taskService.findOneById(USER1_TASK1.getId()));
        Assert.assertNotNull(taskService.findOneById(USER2_TASK1.getId()));
        Assert.assertNotNull(taskService.removeOneByIndex(USER1.getId(), 0));
        Assert.assertNotNull(taskService.findOneById(USER2_TASK1.getId()));
        Assert.assertNull(taskService.findOneById(USER1_TASK1.getId()));

        thrown.expect(UserIdEmptyException.class);
        taskService.removeOneByIndex(NULL_USER_ID, 0);
        thrown.expect(IndexIncorrectException.class);
        taskService.removeOneByIndex(USER1.getId(), -1);
    }

    @Test
    public void testTaskRemoveAll() {
        taskService.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.size(), taskService.findAll().size());
        taskService.removeAll();
        Assert.assertEquals(0, taskService.findAll().size());
    }

    @Test
    public void testTaskRemoveAllByUserId() {
        taskService.add(USER1_TASK_LIST);
        taskService.add(USER2_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.size(), taskService.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_TASK_LIST.size(), taskService.findAll(USER2.getId()).size());
        taskService.removeAll(USER1.getId());
        Assert.assertEquals(0, taskService.findAll(USER1.getId()).size());
        Assert.assertEquals(USER2_TASK_LIST.size(), taskService.findAll(USER2.getId()).size());

        thrown.expect(UserIdEmptyException.class);
        taskService.removeAll(NULL_USER_ID);
    }

    @Test
    public void testTaskGetSize() {
        taskService.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.size(), taskService.getSize());
    }

    @Test
    public void testTaskGetSizeByUserId() {
        taskService.add(USER1_TASK_LIST);
        taskService.add(USER2_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST.size(), taskService.getSize(USER1.getId()));
        Assert.assertEquals(USER2_TASK_LIST.size(), taskService.getSize(USER2.getId()));

        thrown.expect(UserIdEmptyException.class);
        taskService.getSize(NULL_USER_ID);
    }

    @Test
    public void testTaskExistsById() {
        taskService.add(USER1_TASK_LIST);
        Assert.assertTrue(taskService.existsById(USER1_TASK1.getId()));
        Assert.assertFalse(taskService.existsById(USER2_TASK1.getId()));
    }

    @Test
    public void testTaskExistsByIdByUserId() {
        taskService.add(USER1_TASK_LIST);
        taskService.add(USER2_TASK_LIST);
        Assert.assertTrue(taskService.existsById(USER1.getId(), USER1_TASK1.getId()));
        Assert.assertFalse(taskService.existsById(USER2.getId(), USER1_TASK1.getId()));

        thrown.expect(UserIdEmptyException.class);
        taskService.existsById(NULL_USER_ID, USER1_TASK1.getId());
    }

    @Test
    public void testTaskCreate() {
        Assert.assertEquals(0, taskService.findAll(USER1.getId()).size());
        @NotNull final String name = "TASK_TEST";
        @NotNull final String description = "Description";
        Assert.assertNotNull(taskService.create(USER1.getId(), name, description));
        Assert.assertEquals(1, taskService.findAll(USER1.getId()).size());
        @NotNull final Task task = taskService.findOneByIndex(0);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER1.getId(), task.getUser().getId());
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());

        thrown.expect(UserIdEmptyException.class);
        taskService.create(NULL_USER_ID, name, description);
        thrown.expect(NameEmptyException.class);
        taskService.create(USER1.getId(), null, description);
        thrown.expect(DescriptionEmptyException.class);
        taskService.create(USER1.getId(), name, null);
    }

    @Test
    public void testTaskUpdateById() {
        taskService.add(USER1_TASK_LIST);
        @NotNull final String name = "TASK_TEST";
        @NotNull final String description = "Description";
        Assert.assertNotNull(taskService.updateById(USER1.getId(), USER1_TASK2.getId(), name, description));
        @NotNull final Task task = taskService.findOneById(USER1_TASK2.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());

        thrown.expect(UserIdEmptyException.class);
        taskService.updateById(NULL_USER_ID, USER1_TASK2.getId(), name, description);
        thrown.expect(ProjectIdEmptyException.class);
        taskService.updateById(USER1.getId(), NULL_TASK_ID, name, description);
        thrown.expect(NameEmptyException.class);
        taskService.updateById(USER1.getId(), USER1_TASK2.getId(), null, description);
        thrown.expect(DescriptionEmptyException.class);
        taskService.updateById(USER1.getId(), USER1_TASK2.getId(), name, null);
    }

    @Test
    public void testTaskUpdateByIndex() {
        taskService.add(USER1_TASK_LIST);
        @NotNull final String name = "TASK_TEST";
        @NotNull final String description = "Description";
        Assert.assertNotNull(taskService.updateByIndex(USER1.getId(), 1, name, description));
        @NotNull final Task task = taskService.findOneById(USER1_TASK2.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(name, task.getName());
        Assert.assertEquals(description, task.getDescription());

        thrown.expect(UserIdEmptyException.class);
        taskService.updateByIndex(NULL_USER_ID, 1, name, description);
        thrown.expect(IndexIncorrectException.class);
        taskService.updateByIndex(USER1.getId(), -1, name, description);
        thrown.expect(NameEmptyException.class);
        taskService.updateByIndex(USER1.getId(), 1, null, description);
        thrown.expect(DescriptionEmptyException.class);
        taskService.updateByIndex(USER1.getId(), 1, name, null);
    }

    @Test
    public void testTaskChangeTaskStatusById() {
        taskService.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK1.getStatus(), taskService.findOneById(USER1_TASK1.getId()).getStatus());
        Assert.assertNotNull(taskService.changeTaskStatusById(USER1.getId(), USER1_TASK1.getId(), Status.NOT_STARTED));
        Assert.assertEquals(Status.NOT_STARTED, taskService.findOneById(USER1_TASK1.getId()).getStatus());

        thrown.expect(UserIdEmptyException.class);
        taskService.changeTaskStatusById(NULL_USER_ID, USER1_TASK1.getId(), Status.NOT_STARTED);
        thrown.expect(ProjectIdEmptyException.class);
        taskService.changeTaskStatusById(USER1.getId(), NULL_TASK_ID, Status.NOT_STARTED);
        thrown.expect(StatusEmptyException.class);
        taskService.changeTaskStatusById(USER1.getId(), USER1_TASK1.getId(), null);
    }

    @Test
    public void testTaskChangeTaskStatusByIndex() {
        taskService.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK1.getStatus(), taskService.findOneById(USER1_TASK1.getId()).getStatus());
        Assert.assertNotNull(taskService.changeTaskStatusByIndex(USER1.getId(), 0, Status.NOT_STARTED));
        Assert.assertEquals(Status.NOT_STARTED, taskService.findOneById(USER1_TASK1.getId()).getStatus());

        thrown.expect(UserIdEmptyException.class);
        taskService.changeTaskStatusByIndex(NULL_USER_ID, 0, Status.NOT_STARTED);
        thrown.expect(IndexIncorrectException.class);
        taskService.changeTaskStatusByIndex(USER1.getId(), -1, Status.NOT_STARTED);
        thrown.expect(StatusEmptyException.class);
        taskService.changeTaskStatusByIndex(USER1.getId(), 0, null);
    }

    @Test
    public void testTaskFindAllByProjectId() {
        @NotNull final Task task1 = USER1_TASK1;
        @NotNull final Task task3 = USER1_TASK3;
        task1.setProject(USER1_PROJECT1);
        task3.setProject(USER1_PROJECT1);
        taskService.add(USER1_TASK_LIST);
        taskService.add(USER2_TASK_LIST);
        @NotNull final List<Task> taskList1 = taskService.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId());
        @NotNull final List<Task> taskList2 = taskService.findAllByProjectId(USER2.getId(), USER1_PROJECT1.getId());
        Assert.assertEquals(2, taskList1.size());
        Assert.assertEquals(USER1_PROJECT1.getId(), taskList1.get(0).getProject().getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), taskList1.get(1).getProject().getId());
        Assert.assertEquals(0, taskList2.size());
    }

    @Test
    public void testTaskBindUnbindProject() {
        taskService.add(USER1_TASK_LIST);
        Assert.assertNull(taskService.findOneById(USER1_TASK1.getId()).getProject());
        taskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), taskService.findOneById(USER1_TASK1.getId()).getProject().getId());
        taskService.unbindTaskFromProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertNull(taskService.findOneById(USER1_TASK1.getId()).getProject());
    }

}
