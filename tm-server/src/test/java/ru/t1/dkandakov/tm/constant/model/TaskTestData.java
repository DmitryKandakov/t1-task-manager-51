package ru.t1.dkandakov.tm.constant.model;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.model.Task;

import java.util.Arrays;
import java.util.List;

import static ru.t1.dkandakov.tm.constant.model.UserTestData.*;

@UtilityClass
public final class TaskTestData {

    @NotNull
    public final static Task USER1_TASK1 = new Task(USER1, "TASK_01_01", "Test task 1 for user 1.", Status.COMPLETED);

    @NotNull
    public final static Task USER1_TASK2 = new Task(USER1, "TASK_01_02", "Test task 2 for user 1.", Status.IN_PROGRESS);

    @NotNull
    public final static Task USER1_TASK3 = new Task(USER1, "TASK_01_03", "Test task 3 for user 1.", Status.NOT_STARTED);

    @NotNull
    public final static Task USER2_TASK1 = new Task(USER2, "TASK_02_01", "Test task 1 for user 2.", Status.NOT_STARTED);

    @NotNull
    public final static Task USER2_TASK2 = new Task(USER2, "TASK_02_01", "Test task 2 for user 2.", Status.IN_PROGRESS);

    @NotNull
    public final static Task ADMIN_TASK1 = new Task(ADMIN, "TASK_02_02", "Test task 2 for user 2.", Status.COMPLETED);

    @NotNull
    public final static Task ADMIN_TASK2 = new Task(ADMIN, "TASK_03_01", "Test task 1 for admin.", Status.NOT_STARTED);

    @NotNull
    public final static List<Task> USER1_TASK_LIST = Arrays.asList(USER1_TASK1, USER1_TASK2, USER1_TASK3);

    @NotNull
    public final static List<Task> USER2_TASK_LIST = Arrays.asList(USER2_TASK1, USER2_TASK2);

    @NotNull
    public final static List<Task> ADMIN_TASK_LIST = Arrays.asList(ADMIN_TASK1, ADMIN_TASK2);

    @Nullable
    public final static Task NULL_TASK = null;

    @Nullable
    public final static String NULL_TASK_ID = null;

    @Nullable
    public final static List<Task> NULL_TASK_LIST = null;

}
