package ru.t1.dkandakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.domain.DataBase64SaveRequest;
import ru.t1.dkandakov.tm.enumerated.Role;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data to a base64 file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        getDomainEndpoint().saveDataBase64(new DataBase64SaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-base64";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}